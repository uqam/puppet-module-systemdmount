# systemdmount

Puppet module to manage network file system (via systemd mount).
Work in progress.

## Table of Contents

* [Overview](#overview)

* [Usage](#usage)

* [Operating Systems Support](#operating-systems-support)

* [Development](#development)

## Overview

Puppet module to manage network file system (via systemd mount).

## Usage

* Use systemdmount::unit or hash :

```ruby
    class { 'systemdmount': mounts => {
        'mnt2' => {
          server  => '192.168.1.1',
          export  => '/export2',
          where   => '/mnt2',
          options => ''
         },
         'mnt3' => {
           server  => '192.168.1.1',
           export  => '/export3',
           where   => '/mnt3',
           options => ''
         }
        }}

    systemdmount::unit {'mnt1' :    server  => '192.168.666.166',    export  => '/export1',    where   => '/mnt1',    options     => '' }
```

Following systemd usage, service name need to match local path.
ie: Filename should be : "mnt-export.mount" for "/mnt/export".

## Operating Systems Support

This is tested on these OS:

* CentOS 7

## Development

Pull requests (PR) and bug reports via GitHub are welcomed.
