#
# = Define: systemdmount::unit
#
# Currently only Redhat family is supported.
#
# == Parameters
#
# [*description*]
# Text. Default "Mount service for ${name}."
# [*server*]
# Text. Default 192.168.1.1. IP Address or FQDN for server.
# [*export*]
# Text. Default empty. Export path.
# [*where*]
# Text. Default "/mnt". Local Path.
# [*options*]
# Text, Default undef.
# [*type*]
# Text. Default "nfs".
# [*enable*]
# Text. Default undef (false).
#

define systemdmount::unit(
  $description = "Mount service for ${name}.",
  $server = '192.168.1.1',
  $export = undef,
  $where  = '/mnt',
  $options = undef,
  $type = 'nfs',
  $enable = true,
){
      exec { "systemctl-daemon-reload-${name}.mount":
        command     => 'systemctl daemon-reload',
        path        => '/usr/bin',
        refreshonly => true,
      }
      file { "/etc/systemd/system/${name}.mount":
        ensure  => present,
        content => template('systemdmount//etc/systemd/system/units.erb'),
        mode    => '0644',
        owner   => 'root',
        group   => 'root',
        notify  => Exec["systemctl-daemon-reload-${name}.mount"],
      }
      if($enable) {
        service { "${name}.mount":
          ensure => 'running',
          enable => true,
        }
      }
      else {
        service { "${name}.mount":
          ensure => 'stopped',
          enable => false,
        }
      }
}
