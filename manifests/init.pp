#
# = Class: systemdmount
#
# This class configure mount fs via systemd.
#
# == Parameters
#
# [*mounts*]
# Hash. Default undef.
#

class systemdmount (
  $mounts = {},
){
  create_resources("systemdmount::unit",$mounts)
}
